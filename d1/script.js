console.log ("Array Manipulatiion")

// basic array structure
// access elements in an array - through index

// two ways to initialize an array

let array = [1,2,3];
console.log(array);

let arr = new Array (1, 2, 3);
console.log(arr);


// index = array.length - 1
/*console.log(array[0]);
console.log(array[1]);
console.log(array[2]);
*/
// array manipulation

let count = ["one", "two", "three", "four"];

console.log(count.length);
console.log(count[4]);

		// using assignment operator (=)
		count[4] = "five"
		console.log(count)

		// push method array.push()
		count.push("element")
		console.log(count)

		function pushMethod(element) {
			return count.push(element)
		}
		pushMethod("six")
		pushMethod("seven")
		pushMethod("eight")

		

		console.log(count)

		// pop method array.pop()
			// removes the last element of an array

		count.pop("four");

		console.log(count)

		function popMethod(){
			count.pop()
		}
		popMethod()

		console.log(count)

	// unshift method array.unshift()
	count.unshift("hugot")
	console.log(count)

	function unshiftmethod(name) {
		return count.unshift(name)
	}

	unshiftmethod("zero")

	console.log(count)

	// shift method array.shift()
	count.shift()
	console.log(count)

	function shftMethod() {
		count.shift()
	}
	shftMethod()

	console.log(count)


// sort method array.sort()
	let nums = [15, 32, 61, 130, 230, 13, 34];
	nums.sort();

	console.log(nums)

	// sort nums in ascending order
	nums.sort(
			function(a, b){
				return a - b

			}
		)
	console.log(nums)

	// reverse method array.reverse()
	nums.reverse()

	console.log(nums)

/*Splce and Slice */

// Splice method array.splice()
	// returns an array of omitted elements

	// first parameter - index where to start omitting element
	// second parameter - # of elements to be omtted starting from the first parameter
	// third parameter - elements to be added in place of the omitted elements
console.log(count)
/*
let newSplice = count.splice(1);
console.log(newSplice)
console.log(count)*/

// let newSplice = count.splice(3, 2);
// console.log(newSplice);
// console.log(count)

// let newSplice = count.splice(1,2, "a1", "b2", "b3")
// 	console.log(newSplice)
// 	console.log(count)



	// Slice method array.slice(start, end)

	// first parameter - index where to begin omitting elements

	// second parameter - # of elements to be omitted (index - 1)



	// console.log(count)

	// let newSlice = count.slice(3);
	// console.log(newSlice);
	// console.log(count)
	

	console.log(count)

	let newSlice = (1, 5);
	console.log(newSlice);

	// Concat method
			// used to merge two or more arrays array.concat()
		console.log(count)
		console.log(nums)
		let animals = ["bird", "cat", "dog", "fish"]


		let newConcat = count.concat(nums, animals)
		console.log(newConcat);
	// Join method

		let meal = ["rice", "steak", "juice"];
		// let newJoin = meal.join("")
		// console.log(newJoin)

		let newJoin = meal.join("_")
		console.log(newJoin)


	// toStringmethod
	console.log(nums)
	console.log(typeof nums [3])

	let newString = nums.toString()
	console.log(newString)

// accessors
let countries = ["US","PH","CAN","SG","HK","PH","NZ"];
/*// indexOf() array.indexOf()
		// finds the index of a given element where it is
		"first" found.*/
let index = countries.indexOf("PH")
console.log(index);

// if element is non existent, return is -1
let example = countries.indexOf("AU")
console.log(example);

//lastIndexOf()
let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);


if(countries.indexOf("AU") == -1){
	console.log('element non existing')

} else {
	console.log('element exists in countries array')
}

/*Iterators*/

	// forEach(cb())     array.forEach()
	// map() 		array.map()

let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"]

	days.forEach(
		function(element) {
			console.log (element)
		}
	)

	// map
	// 	return a copy of an array from the original array which can be manipulated
	// let mapdays = days.map(function(day) {
	// 	return `${day} is the day of the week`
	// })

	// 	console.log(mapdays)
	// 	console.log(days)


		let array2 = days.map(function (day) {
			return (day)
		})

		console.log(array2)


		let days2 = [];
		days.forEach(function (element) {
			days2.push(element);
		})
		console.log(days2);


// filter      array.filter(cb())
	console.log(nums)

	let newFilter = nums.filter(function(num){
		return num < 50
	})

		console.log(newFilter)

		//includes   array.includes
		console.log(animals)

		let newIncludes = animals.includes("dog")
		console.log (newIncludes)



function miniactivity(name) {
	if (animals.includes(name) == true){
		return `${name} is found`
	} else {
		return `${name} is not found`
	}
}


console.log (miniactivity("cat"))
console.log (miniactivity("dragon"))
console.log (miniactivity("ant"))



	//every
		//boolean
		/*returns true only if "all elements" passed the given condition*/

		let newEvery = nums.every(function(num){
				return ( num > 1)
		})

		console.log (newEvery)

		// some (cb())
			// boolean
		let newSome = nums.some(function(num){
			return ( num > 50 )
		})
		console.log (newSome);

		//  let newSome2 = nums.some(num => num > 50)
		// console.log(newSome2);

		// reduce (cb(<previous>, <current>))
		let newreduce = nums.reduce(function(a, b){

			return a + b
			// return b - a
		})
		console.log (newreduce) 

		// to get the average of nums array
			// total all the elements
			//  divide it by total number of elements
		let average = newreduce/ nums.length
// ___________________________________________________
		// toFixed(# of decimals) - returns string

		console.log (average.toFixed(2))

		// parseInt() or parseFloat ()
		console.log(parseInt(average))

		console.log(parseInt(average.toFixed(2)))
		console.log(parseFloat(average.toFixed(2)))
